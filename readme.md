# WifiTracker to track probes from devices around AP with ESP8266
This simple set of programs allows to track probes from devices to AP and save them into remote database using ESP8266 module. Purpose of this is mainly for tracking people! :D

## TODO:
- create simple API for logging these data
- make ESP8266 log these MACaddresses and send them after a while to api 
..- JSON would be nice
..- Buckle up MAC address to compact way (use bytes not string)
